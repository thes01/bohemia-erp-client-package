# Installing the package

Add following to your composer.json

~~~
"require": {
    "thes01/bohemia-erp-client-package": "0.2.*"
},

"repositories": [
    {
        "type": "vcs", 
        "url": "https://gitlab.com/thes01/bohemia-erp-client-package"
    }
],
~~~

then run

~~~
composer update
php artisan vendor:publish --provider="Bohemia\ERP\BohemiaERPServiceProvider"
~~~

and finally, add config values to the .env file

~~~
ERP_API_TOKEN= #api_token of a user in ERP
ERP_API_ENDPOINT= #url of the ERP instance
~~~

do not forget to *php artisan config:cache* after setting up the credentials





# IMPORTANT
for PHP 8.+ , use version 0.1.*
for PHP 7.+ , use version 0.2.*

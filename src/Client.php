<?php

namespace Bohemia\ERP;

use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ClientException;

use Bohemia\ERP\Models\Order;
use Bohemia\ERP\Models\OrderItem;
use Bohemia\ERP\Models\Recipient;

use Exception;

use Bohemia\ERP\Exceptions\ERPApiException;
use Bohemia\ERP\Models\Transaction;

class Client
{
    protected $client;
    protected $api_token;

    public function __construct()
    {
        $this->api_token = config('bohemia_erp.api_token');

        $this->client = new HttpClient([
            'base_uri' => config('bohemia_erp.api_endpoint'),
            'headers' => [
                'Authorization' => "Bearer " . $this->api_token,
                'Accept' => 'application/json'
            ]
        ]);
    }

    private function request(string $method, string $endPoint, array $params = [])
    {
        try {
            $response = $this->client->request($method, $endPoint, $params);
        } catch (RequestException $ex) {
           // $resp = $ex->getResponse();
            //$data = json_decode($resp->getBody()->getContents());

            throw new ERPApiException($ex->getMessage());
            //throw new ERPApiException($data->status . ' (' . $resp->getStatusCode() . ')', $resp->getStatusCode());
        }

        // other possible exceptions: GuzzleHttp\Exception\ServerException (504: timed out)
        // but those should be catched in the app...I guess?

        return json_decode($response->getBody()->getContents());
    }

    public function post(string $endPoint, array $params = [])
    {
        return $this->request('post', $endPoint, ['form_params' => $params]);
    }

    public function patch(string $endPoint, array $params = [])
    {
        return $this->request('patch', $endPoint, ['form_params' => $params]);
    }

    public function delete(string $endPoint, array $params = [])
    {
        return $this->request('delete', $endPoint, $params);
    }

    public function get(string $endPoint, array $params = [])
    {
        return $this->request('get', $endPoint, $params);
    }

    public function getHttpClient()
    {
        return $this->client;
    }

    public function storeOrder(Order $order, int $company_id, int $recipient_id)
    {
        return $this->post(
            'api/orders',
            array_merge(compact('company_id', 'recipient_id'), $order->getArray())
        );
    }

    public function updateOrder(int $id,array $order)
    {
        return $this->patch(
            "api/orders/$id",
            $order
        );
    }
    public function updateOrderPaymentStatus(int $id, array $order)
    {
        return $this->get("api/orders/$id/update-payment-status");
    }

    public function getOrders(int $company_id)
    {
        return $this->get("api/orders?company=$company_id");
    }

    public function getOrder(int $id)
    {
        return $this->get("api/orders/$id");
    }

    public function getInvoice(int $id)
    {
        return $this->get("api/invoices/$id");
    }

    public function getOrderIterationsWithAllData(int $id)
    {
        return $this->get("api/order/$id/iterations-with-all-data");
    }

    public function getOrderItems(int $id)
    {
        return $this->get("api/order-items/order/$id");
    }

    public function confirmStagingOrder(int $id)
    {
        return $this->post("api/orders/$id/confirm");
    }

    public function deleteStagingOrder(int $id)
    {
        return $this->delete("api/orders/$id");
    }

    public function createRecipient(Recipient $recipient, int $company_id)
    {
        return $this->post(
            'api/recipients',
            array_merge(compact('company_id'), $recipient->getArray())
        );
    }

    public function storeOrderItem(OrderItem $item, int $order_id)
    {
        return $this->post(
            'api/order_items',
            array_merge(compact('order_id'), $item->getArray())
        );
    }

    public function getInvoiceFileUrl(int $invoice_id)
    {
        return config('bohemia_erp.api_endpoint') . "/api/invoices/show/$invoice_id/$this->api_token";
    }

    public function getRecipientsInvoiceFileUrl(int $invoice_id, string $token)
    {
        return config('bohemia_erp.api_endpoint') . "/api/invoices/file/$invoice_id/$token";
    }

    public function getInvoiceFile(int $invoice_id)
    {
        $url = $this->getInvoiceFileUrl($invoice_id);

        $response = $this->client->get($url)->getBody();
        return $response;
    }

    public function updateRecipient(Recipient $recipient, int $recipient_id, int $company_id)
    {
        return $this->patch(
            'api/recipients/' . $recipient_id,
            array_merge(compact('company_id'), $recipient->getArray())
        );
    }

    public function storeTransaction(int $type, $amount, int $order_id, int $company_id)
    {
        return $this->post('api/transactions/', compact('company_id', 'type', 'amount', 'order_id'));
    }

    // public function storeTransaction(Transaction $transaction, $order_id, $company_id)
    // {
    //     return $this->post('api/transactions',
    //         array_merge(compact('company_id', 'order_id'), $transaction->getArray()));
    // }

    // public function getTransaction($id) {

    // }

    public function getRecipient($recipient_id)
    {
        return $this->get("api/recipients/$recipient_id");
    }
}

<?php

namespace Bohemia\ERP\Api;

use Bohemia\ERP\Client;

class AbstractApi
{
    protected $client;

    protected $endpoint;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }
}
<?php

namespace Bohemia\ERP\Models;

class Order extends AbstractModel
{
    public $title;

    // 0 -> Request, than invoice
    // 1 -> Only invoice with due date
    // 2 -> Invoice and request
    public $invoicing_model; // required

    public $renewable;
    public $renew_unit;
    public $renew_account;
    public $currency; // default: czk, possible: czk, eur and usd

    // nullable field, to which a request is sent from ERP
    // when the order status is changed
    public $callback_url;
    public $invoice_language;
    public $country_zone;
    public $is_moss;
    public $wished_variable_symbol;
    public $is_render_locked;

    protected $fields = [
        'title', 'invoicing_model', 'renewable', 'renew_unit',
        'renew_account', 'currency', 'callback_url', 'invoice_language', 'country_zone',
        'is_moss', 'wished_variable_symbol', 'is_render_locked'
    ];
}

<?php

namespace Bohemia\ERP\Models;

class Transaction extends AbstractModel {
    public $type; // required
    public $amount;
    public $currency;

    protected $fields = ['type', 'amount', 'currency'];

    const TYPE_MANUAL_BANK_TRANSFER = 0;
    const TYPE_FAKE = 1;
    const TYPE_CASH = 2;
    const TYPE_GENERIC_TRANSACTION = 90;
    const TYPE_BITPAY = 10;
    const TYPE_PAYPAL = 11;
    const TYPE_PAYS_CZ = 12;
    const TYPE_GOPAY = 13;
    const TYPE_FIO_API = 30;
    const TYPE_KB_API = 31;
    const TYPE_CSOB_API = 32;
    const TYPE_CP_API = 33;
}
<?php

namespace Bohemia\ERP\Models;

class Recipient extends AbstractModel {
    // 0 => 'individual_person',   // FO
    // 1 => 'legal_person',        // PO - podnikatel
    // 2 => 'company'
    public $type; // required

    public $company; // req if type is company
    public $name; // req
    public $surname; // req
    public $street;
    public $town;
    public $postal;
    public $country;
    public $ic;
    public $dic;
    public $email;
    public $phone;

    protected $fields = ['type', 'company', 'name', 'surname', 'street',
    'town', 'postal', 'country', 'ic', 'dic', 'email', 'phone'];


}
<?php

namespace Bohemia\ERP\Models;

abstract class AbstractModel {
    protected $fields;

    public function __construct($params) {
        if (is_array($params)) {
            foreach ($this->fields as $param) {
                 $this->{$param} = isset($params[$param]) ? $params[$param] : NULL;
            }
        }
    }

    public function getArray()
    {
        $arr = [];
        foreach ($this->fields as $field) {
            $arr += [$field => $this->{$field}];
        }

        return $arr;
    }
}
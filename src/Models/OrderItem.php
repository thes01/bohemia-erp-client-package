<?php

namespace Bohemia\ERP\Models;
class OrderItem extends AbstractModel {
   public $code; // req
   public $description; // req
   public $price;
   public $vat_rate; // req
   public $subject_id;
   public $is_price_with_vat; // default: false
   public $qty;

   protected $fields = ['code', 'description', 'price', 'vat_rate','subject_id', 'is_price_with_vat', 'qty'];
}
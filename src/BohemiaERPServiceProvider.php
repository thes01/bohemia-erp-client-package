<?php

namespace Bohemia\ERP;
use Illuminate\Support\ServiceProvider;

class BohemiaERPServiceProvider extends ServiceProvider
{
    /**
    * Publishes configuration file.
    *
    * @return  void
    */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/../config/bohemia_erp.php' => config_path('bohemia_erp.php'),
        ], 'bohemia-erp-config');
    }
    /**
    * Make config publishment optional by merging the config from the package.
    *
    * @return  void
    */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__.'/../config/bohemia_erp.php',
            'bohemia_erp'
        );
    }
}
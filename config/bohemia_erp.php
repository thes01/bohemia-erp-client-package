<?php
return [
    'api_token' => env('ERP_API_TOKEN'),
    'api_endpoint' => env('ERP_API_ENDPOINT')
];